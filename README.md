# Button Box

A button box for kids: source code, design, and hardware.

## Dependencies

To build and run you'll need:

- Rust >=1.69 with `thumbv7em-none-eabi` target installed.

- `openocd`

``` console
$ rustup target add thumbv7em-none-eabi thumbv7em-none-eabihf
```

## Building and running

1. Start `openocd` is a seperate console.

2. Build the application.

``` console
$ cargo build
```

3. Run.

```console
$ cargo run
```

# Copying

This template is licensed under [MIT](LICENSE).


