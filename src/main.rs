#![cfg_attr(test, allow(unused_imports))]
#![cfg_attr(not(test), no_std)]
#![cfg_attr(not(test), no_main)]

extern crate alloc;

// pick a panicking behavior
#[cfg(not(test))]
use panic_halt as _; // you can put a breakpoint on `rust_begin_unwind` to catch panics
                     // use panic_abort as _; // requires nightly
                     // use panic_itm as _; // logs messages over ITM; requires ITM support
                     // use panic_semihosting as _; // logs messages to the host stderr; requires a debugger

use cortex_m::asm;
use cortex_m_rt::entry;
use cortex_m_semihosting::hprintln;
use stm32f7::stm32f7x2::Peripherals;

use alloc::vec;
use core::mem::MaybeUninit;
use embedded_alloc::Heap;

#[global_allocator]
static HEAP: Heap = Heap::empty();

#[cfg(not(test))]
#[entry]
fn main() -> ! {
    // Initialise the allocator
    {
        const HEAP_SIZE: usize = 512;
        static mut HEAP_MEM: [MaybeUninit<u8>; HEAP_SIZE] = [MaybeUninit::uninit(); HEAP_SIZE];
        unsafe { HEAP.init(HEAP_MEM.as_ptr() as usize, HEAP_SIZE) }
    }

    let p = Peripherals::take().unwrap();
    p.RCC.ahb1enr.write(|w| w.gpioben().set_bit());
    p.GPIOB.moder.write(|w| w.moder7().bits(0b01));

    hprintln!("Hello world!");

    let xs = vec![0, 1, 2];

    hprintln!("{:?}", xs);

    loop {
        p.GPIOB
            .odr
            .modify(|r, w| w.odr7().bit(r.odr7().bit_is_clear()));

        for _ in 0..100000 {
            asm::nop();
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn foo() {
        println!("tests work!");
        assert!(false);
    }
}
